## Tentang Aplikasi
```
- User bisa melakukan scroll terus menerus hingga mencapai maksimum jumlah data
- User dapat melihat foto produk
- User bisa melihat detail produk
- User bisa melihat produk pada tiap kategori
- User tidak bisa klik tombol Beli jika barang kosong

saya menggunakan url gambar random, mohon maaf jika saat ada gambar yang clickable waktu diklik gambarnya yang muncul tidak sesuai waktu sebelum di klik hal ini terjadi di chrome, di mozzila aman
```

## Setting Env
```
move to this project, run command => cp .env-sample .env (or just make .env file in root project)
set your .env like this
NODE_PATH=src/
NODE_ENV=development
REACT_APP_API_ENDPOINT=http://localhost:9999/api (change port localhost with backend port)
```

## Run dev
```
yarn install
yarn build-css
yarn start
```

## Run production
```
yarn install
yarn build-css
yarn build
yarn global add serve (if serve not installed yet)
serve -s build
```