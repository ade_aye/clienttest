import { combineReducers } from 'redux';
import category from './Category';
import product from './Product';

export default combineReducers({
	category,
	product,
})
