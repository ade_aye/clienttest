import * as types from 'reduxFolder/actions/Category/actionTypes';

const initialState = {
	list:[],
}

export default (state = initialState, action) =>{
	const { payload } = action;
	switch (action.type) {
	    case types.LIST_CATEGORY: {
	      return {
	        ...state,
	        list: payload,
	      }
	    }
		default:
      		return state
	}
}
