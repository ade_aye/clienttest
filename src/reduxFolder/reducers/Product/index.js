import * as types from 'reduxFolder/actions/Product/actionTypes';

const initialState = {
	data:{},
}

export default (state = initialState, action) =>{
	const { payload } = action;
	switch (action.type) {
	    case types.LIST_PRODUCT: {
	      return {
	        ...state,
	        data: payload,
	      }
	    }
		default:
      		return state
	}
}
