import * as types from './actionTypes';
import {apiEndpoint} from 'utils/api';
import _ from 'lodash';

export const getProduct=(params='', reset=false, callback)=> async(dispatch,getState)=>{
	const options = {
		method: 'GET',
	};

	const response = await fetch(`${apiEndpoint}/product${params}`, options)

	const json = await response.json();
	if (json.error) {

		if (callback) {
			callback(false,json)
		}

	}
	else {

		let productList = getState().product.data;
		let result = json.result;
		if (productList.data && reset === false) {
			let merge = productList.data.concat(result.data);
			merge = _.uniqBy(merge,'id');
			result={
				...result,
				data:merge
			}
		}
		dispatch({
			type:types.LIST_PRODUCT,
			payload: result,
		});

		if (callback) {
			callback(true,json.result)
		}
	}
}