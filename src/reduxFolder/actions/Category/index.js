import * as types from './actionTypes';
import {apiEndpoint} from 'utils/api';

export const listCategory=(callback)=> async(dispatch,getState)=>{
	const options = {
		method: 'GET',
	};

	const response = await fetch(`${apiEndpoint}/category`, options)

	const json = await response.json();

	if (json.error) {

		if (callback) {
			callback(false,json)
		}

	}
	else {
		dispatch({
			type:types.LIST_CATEGORY,
			payload: json.result,
		});

		if (callback) {
			callback(true,json.result)
		}
	}
}