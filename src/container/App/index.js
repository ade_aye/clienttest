import React, { Component } from 'react';
import { Switch, Route, withRouter } from 'react-router-dom'

import PageNotFound from './components/PageNotFound'
import Homepage from './components/Homepage/index.js'

class App extends Component {
	render() {
		return (
      <div>
        <Switch>
          <Route exact path="/" component={Homepage}/>
          <Route component={PageNotFound} />
        </Switch>
      </div>
		);
	}
}

export default withRouter(App);
