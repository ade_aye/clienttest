import React from 'react'

const CategoryCircleIcon=({data,selectedCategory, setSelectedCategory})=>{
	return(
		<div className="list-icon-wrapper">
			{data.map(category=>{
				return(
					<div key={category.id}>
						{category.name !== selectedCategory && <div className="category-icon" onClick={()=>setSelectedCategory(category.name,category.id)}>{category.name}</div>}
					</div>)
			})}
			{selectedCategory !== 'semua'?<div className="category-icon" onClick={()=>setSelectedCategory('semua')}>Semua</div>:''}
		</div>)
}

export default CategoryCircleIcon;