import React from "react";
import CategoryCircleIcon from './CategoryCircleIcon';

const Category = ({isShow,toggleCategory,data,selectedCategory, setSelectedCategory}) => {
  return (
    <div className="category">
      <div className="category__top">
        <div className="selected-category">Kategori : {selectedCategory}</div>
        <div className="select-category" onClick={toggleCategory}>
          Pilih kategori lainnya <i className={isShow?'fas fa-caret-up':'fas fa-caret-down'} />
        </div>
      </div>
      {isShow &&
      	<div className={`category__content`}>
			<CategoryCircleIcon
			data={data}
      selectedCategory={selectedCategory}
      setSelectedCategory={setSelectedCategory}
			/>
      </div>}
      
    </div>
  );
};


export default Category;