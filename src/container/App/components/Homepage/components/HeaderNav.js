import React from "react";

const HeaderNav = ({value,handleChange}) => {
  return (
    <div className="top-navigation">
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-6 col-md-offset-3">
            <img
              src="https://www.salestockindonesia.com/assets/images/logo-ss-34f2d4dd.png"
              className={"top-navigation__image"}
              alt=""
            />
            <div className="action">
              <div className="action__search">
                <div className="form-inline">
                  <div className="form-group">
                    <div className="input-group">
                      <div className="input-group-addon">
                        <i className="fas fa-search" />
                      </div>
                      <input
                        type="text"
                        className="form-control"
                        name={"searchValue"}
                        value={value}
                        onChange={handleChange}
                        placeholder="Cari Kesukaanmu"
                      />
                    </div>
                  </div>
                </div>
              </div>
              <button className="btn action__chart">
                <i className="fas fa-shopping-cart" />
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};


export default HeaderNav;