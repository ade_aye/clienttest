import React from "react";
import ImageZoom from "react-medium-image-zoom";

class ProductCard extends React.Component {
  constructor() {
    super();
    this.state = {
      mainImage: ""
    };
  }
  componentWillMount = () => {
    this.setState({
      mainImage: this.props.data.photos
        ? this.props.data.photos[0].url
        : "/img/noimg.jpg"
    });
  };
  setMainImage = url => {
    this.setState({
      mainImage: url
    });
  };
  render() {
    const {
      data,
      data: { detail, photos, category, name },
      simple,
      ...props
    } = this.props;
    return (
      <div
        className={`product ${
          simple && detail.stock === 0 ? "product--empty" : ""
        } `}
      >
        <div className="product__photo">
          {simple ? (
            <ImageZoom
              image={{
                src: photos ? photos[0].url : "/img/noimg.jpg",
                alt: "Product",
                className: "img-responsive main"
              }}
              zoomImage={{
                src: photos ? photos[0].url : "/img/noimg.jpg",
                alt: "Product"
              }}
            />
          ) : (
            <img
              src={this.state.mainImage}
              alt="Product"
              className="img-responsive main"
            />
          )}
          {!simple && (
            <div className="mini-wrapper">
              {photos.map(photo => {
                return (
                  <div className="mini-photo" key={photo.id}>
                    <img
                      src={photo.url}
                      alt=""
                      className="img-responsive"
                      onClick={() => this.setMainImage(photo.url)}
                    />
                  </div>
                );
              })}
            </div>
          )}
        </div>
        <div className="product__description">
          <h5>{name}</h5>
          {detail.discount_price ? (
            <h6>
              <del>Rp {detail.normal_price.toLocaleString("de-DE")}</del>{" "}
              <span>Rp {detail.discount_price.toLocaleString("de-DE")}</span>
            </h6>
          ) : (
            <h6>Rp {detail.normal_price.toLocaleString("de-DE")}</h6>
          )}

          {simple && (
            <button
              className="btn show-more"
              onClick={() => this.props.openModal(this.props.data)}
            >
              Detailnya Sis!
            </button>
          )}
        </div>
        {!simple && (
          <div className="product__more-description">
            <p>{detail.description}</p>

            <table className="table table-bordered">
              <tbody>
                <tr>
                  <td>Kategori</td>
                  <td>{category.name}</td>
                </tr>
                <tr>
                  <td>Merek</td>
                  <td>{detail.brand}</td>
                </tr>
                <tr>
                  <td>Ukuran</td>
                  <td>{detail.size}</td>
                </tr>
                <tr>
                  <td>Warna</td>
                  <td>{detail.color}</td>
                </tr>
                <tr>
                  <td>Stok</td>
                  <td>{detail.stock}</td>
                </tr>
              </tbody>
            </table>
          </div>
        )}
        {simple && detail.stock <= 5 ? (
          <div className="product__stock">
            {detail.stock === 0 ? (
              <h5>Stock Kosong sis,Wishlish aku ya</h5>
            ) : (
              <h5>Buruan Sis! Stock tinggal {detail.stock}</h5>
            )}
          </div>
        ) : (
          ""
        )}
        <div className="product__actions">
          <div
            className="btn-group btn-group-justified btn-group-action"
            role="group"
          >
            <div className="btn-group" role="group">
              <button type="button" className="btn btn--left">
                Wishlish
              </button>
            </div>
            <div className="btn-group" role="group">
              <button
                type="button"
                className="btn btn--right"
                disabled={detail.stock === 0 ? true : false}
              >
                Beli
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

ProductCard.defaultProps = {
  simple: true,
  data: {
    photos: []
  },
  detail: {}
};

export default ProductCard;
