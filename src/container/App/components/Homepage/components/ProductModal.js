import React from "react";
import { Modal } from "react-bootstrap";
import ProductCard from "./ProductCard";

const ProductModal = ({ isOpen, toggleModal, modalData }) => {
  return (
    <Modal
      show={isOpen}
      onHide={toggleModal}
      aria-labelledby="contained-modal-title"
    >
      <Modal.Header closeButton>
        <Modal.Title id="contained-modal-title">Product Modal</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="container-fluid">
          <div className="row">
            <div className="col-md-6 col-md-offset-3">
              <ProductCard simple={false} data={modalData} />
            </div>
          </div>
        </div>
      </Modal.Body>
    </Modal>
  );
};

export default ProductModal;
