import React from "react";
import InfiniteScroll from "react-infinite-scroller";
import ProductListLoader from "./ProductListLoader";
import ProductCard from './ProductCard';

const ProductList = ({ data, loadProduct, hasMoreProduct, openModal }) => {
  return (
    <InfiniteScroll
      pageStart={0}
      loadMore={loadProduct}
      hasMore={hasMoreProduct}
      threshold={10}
      loader={<ProductListLoader key={Math.floor(Math.random() * 1001)} />}
    >
        {data.map((product,index) => {
          return (
            <ProductCard
            data={product}
			openModal={openModal}
			key={index}
            />
          );
        })}
    </InfiniteScroll>
  );
};

ProductList.defaultProps = {
  data: []
};
export default ProductList;
