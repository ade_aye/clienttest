import React from "react";
import Category from "./components/Category";
import HeaderNav from "./components/HeaderNav";
import ProductList from "./components/ProductList";
import ProductModal from "./components/ProductModal";

import { bindActionCreators } from "redux";
import { connect } from "react-redux";

import { listCategory } from "reduxFolder/actions/Category";
import { getProduct } from "reduxFolder/actions/Product";

class Homepage extends React.Component {
  constructor() {
    super();
    this.state = {
      searchValue: "",
      isShowCategory: false,
      endPage: false,
      otherParams: "",
      isModalOpen: false,
      activePage: 1,
      modalData: {},
      selectedCategory: "semua"
    };
  }
  onSearchChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };
  setSelectedCategory = (selected, id) => {
    this.setState(
      {
        selectedCategory: selected,
        otherParams: selected === "semua" ? "" : `categoryId=${id}`
      },
      () => this.fetchProduct(1, 2, this.state.otherParams, true)
    );
  };
  toggleCategory = () => {
    this.setState({
      isShowCategory: !this.state.isShowCategory
    });
  };
  componentWillMount = () => {
    this.fetchCategory();
    this.fetchProduct();
  };
  fetchCategory = () => {
    this.props.dispatchListCategory();
  };
  fetchProduct = (page = 1, limit = 2, otherParams = "", reset = false) => {
    this.props.dispatchGetProduct(
      `?page=${page}&limit=${limit}&${otherParams}`,
      reset,
      this.afterFetchProduct
    );
  };
  afterFetchProduct = (success, data) => {
    if (success) {
      this.setState({
        activePage: data.current_page,
        endPage: data.current_page >= data.last_page ? true : false
      });
    }
  };
  toggleModal = data => {
    if (!this.state.isModalOpen) {
      this.setState({
        modalData: data
      });
    }
    this.setState({
      isModalOpen: !this.state.isModalOpen
    });
  };
  render() {
    return (
      <div>
        <HeaderNav
          value={this.state.searchValue}
          handleChange={this.onSearchChange}
        />
        <div className="content">
          <div className="row">
            <div className="col-md-6 col-md-offset-3">
              <Category
                toggleCategory={this.toggleCategory}
                isShow={this.state.isShowCategory}
                data={this.props.category}
                selectedCategory={this.state.selectedCategory}
                setSelectedCategory={this.setSelectedCategory}
              />

              <ProductList
                data={this.props.product.data}
                loadProduct={() =>
                  this.fetchProduct(
                    this.state.activePage + 1,
                    2,
                    this.state.otherParams
                  )
                }
                hasMoreProduct={!this.state.endPage}
                openModal={this.toggleModal}
              />

              <ProductModal
                isOpen={this.state.isModalOpen}
                toggleModal={this.toggleModal}
                modalData={this.state.modalData}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Homepage.defaultProps = {
  category: []
};

const mapStateToProps = state => ({
  category: state.category.list,
  product: state.product.data
});

const matchDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      dispatchListCategory: listCategory,
      dispatchGetProduct: getProduct
    },
    dispatch
  );
};

export default connect(mapStateToProps, matchDispatchToProps)(Homepage);
